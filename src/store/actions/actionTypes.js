export const AUTH_START = 'AUTH_START';
export const AUTH_SUCCESS = 'AUTH_SUCCESS';
export const AUTH_FAIL = 'AUTH_FAIL';
export const AUTH_LOGOUT = 'AUTH_LOGOUT';

export const SET_AUTH_REDIRECT_PATH = 'SET_AUTH_REDIRECT_PATH';

export const FETCH_GIFTS = 'FETCH_GIFTS';
export const SET_GIFTS = 'SET_GIFTS';
export const SET_GIFT = 'SET_GIFT';
export const FETCH_GIFTS_FAILED = 'FETCH_GIFTS_FAILED';



