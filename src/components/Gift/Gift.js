import React, {Component} from 'react';
import {withRouter, Route} from 'react-router-dom';


import classes from './Gift.css';
import SingleGift from '../../containers/Gifts/SingleGift/SingleGift';
import Spinner from '../../components/UI/Spinner/Spinner';

class Gift extends Component {
     giftClickedHandler = () => {
         this.props.history.replace('/gift/' + this.props.data.id);
    };
    render() {
        const placeholder = require('../../assets/images/placeholder.png');
        const thumbnail = require('../../assets/images/'+ this.props.data.categoryName + '/'+ this.props.data.specName + '/' + this.props.data.thumbnail);
        return (
            <div>
                <div onClick={() => this.giftClickedHandler()} className={classes.Gift}>
                    <img src={thumbnail||placeholder} />
                    <span className={classes.Name}> {this.props.data.itemName} ({this.props.data.price}€)</span>
                    <div className={classes.Desc}> {this.props.data.itemDesc}</div>
                </div>
                <Route
                    path={this.props.match.path + '/gift/:id'}
                    component={SingleGift}/>
            </div>

        );
    }
}

export default withRouter(Gift);