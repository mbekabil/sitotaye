import React, { Component } from 'react';
import { Route, Switch, withRouter, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

import Layout from './hoc/Layout/Layout';
import Auth from './containers/Auth/Auth';
import Logout from './containers/Auth/Logout/Logout';
import Gifts from './containers/Gifts/Gifts';
import SingleGift from './containers/Gifts/SingleGift/SingleGift';

import * as actions from "./store/actions";

class App extends Component {
    componentDidMount () {
        this.props.onTryAutoSignup();
    }

    render () {
        let routes = (
            <Switch>
                <Route path="/auth" component={Auth} />
                <Route path="/gift/:id" component={SingleGift}/>
                <Route path="/" exact component={Gifts} />
                <Redirect to="/" />
            </Switch>
        );
        if ( this.props.isAuthenticated ) {
            routes = (
                <Switch>
                    <Route path="/checkout" component={null} />
                    <Route path="/logout" component={Logout} />
                    <Route path="/gift/:id" component={SingleGift}/>
                    <Route path="/auth" component={Auth} />
                    <Route path="/" exact component={Gifts} />
                    <Redirect to="/" />
                </Switch>
            );
        }

        return (
            <div>
                <Layout>
                    {routes}
                </Layout>
            </div>
        );
    }
}
const mapStateToProps = state => {
    return {
        isAuthenticated: state.auth.token !== null
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onTryAutoSignup: () => dispatch( actions.authCheckState() )
    };
};

export default withRouter( connect( mapStateToProps, mapDispatchToProps )( App ) );