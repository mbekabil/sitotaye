import axios from 'axios';

const instance = axios.create({
        baseURL: 'https://sitotaye-ed704.firebaseio.com/'
});

export default instance;