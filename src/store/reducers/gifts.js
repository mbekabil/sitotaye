import * as actionTypes from '../actions/actionTypes';
import {updateObject} from "../../shared/utility";

const initialState = {
    items: null,
    singleItem: null,
    error: false,
};

const setGiftItems = (state, action) => {
    return updateObject( state, {
        items: action.items,
        error: false,
    } );
};

const setSingleGiftItem = (state, action) => {
    return updateObject( state, {
        singleItem: action.singleItem,
        error: false,
    } );
};

const fetchItemsFailed = (state, action) => {
    return updateObject( state, { error: true } );
};

const reducer = ( state = initialState, action ) => {
    switch ( action.type ) {
        case actionTypes.SET_GIFTS: return setGiftItems(state, action);
        case actionTypes.FETCH_GIFTS_FAILED: return fetchItemsFailed(state, action);
        case actionTypes.SET_GIFT: return setSingleGiftItem(state, action);
        default: return state;
    }
};
export default  reducer;