import firebase from 'firebase';
// Required for side-effects
require("firebase/firestore");

const config = {
    apiKey: "AIzaSyDEWeaJO0he2xenW-Om-iug7xlG7iWotXI",
    authDomain: "sitotaye-ed704.firebaseapp.com",
    databaseURL: "https://sitotaye-ed704.firebaseio.com",
    projectId: "sitotaye-ed704",
    storageBucket: "sitotaye-ed704.appspot.com",
    messagingSenderId: "695974167800"
};

firebase.initializeApp(config);

// Initialize Cloud Firestore through Firebase
const db = firebase.firestore();

export default db;