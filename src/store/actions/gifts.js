import * as actionTypes from './actionTypes';
import db from '../../firebase';

let fetchItemsCompleted = false;
export const setItems = (items) => {
    return {
        type: actionTypes.SET_GIFTS,
        items: items
    }
};

export const setSingleItem = (item) => {
    return {
        type: actionTypes.SET_GIFT,
        singleItem: item
    }
};

export const fetchItemsFailed = () => {
    return {
        type: actionTypes.FETCH_GIFTS_FAILED
    }
};

const fetchCategory = (dispatch, db, items, categoryId) => {
    const category = db.collection('category').doc(categoryId);
    category.get()
        .then( ctgSnapshot => {
            const categoryName = ctgSnapshot.data().categoryName;
            items = {
                ...items,
                categoryName
            };
        })
        .catch( error => {
            console.log(error.message);
            dispatch(fetchItemsFailed());
        });
};

const fetchSpecification = (dispatch, db, items, specId) => {
    const spec = db.collection('specification').doc(specId);
    spec.get()
        .then( specSnapshot => {
            const specName = specSnapshot.data().specName;
            items = {
                ...items,
                specName
            };
            console.log(items);
        })
        .catch( error => {
            console.log(error.message);
            dispatch(fetchItemsFailed());
        });
};

export const initGifts = () => {
    //console.log(db.collection('items').get());
    return dispatch => {
        const docRef = db.collection('items');
        let items = [];
        docRef.get()
            .then( querySnapshot => {
                querySnapshot.forEach(doc => {
                    if( doc && doc.data() ) {
                        //fetchCategory(dispatch, db, items, doc.data().categoryId);
                        //fetchSpecification(dispatch, db, items, doc.data().specId);
                        items.push({
                            ...doc.data(),
                            id: doc.id
                        });

                    }
                });
                dispatch(setItems(items));

            })
            .catch( error => {
                console.log(error.message);
                dispatch(fetchItemsFailed());
            });
    };
};

export const setSingleGift = (id) => {
  return dispatch => {
      const docRef = db.collection('items').doc(id);
      docRef.get().then( doc => {
          if (doc.exists) {
              console.log("Document data:", doc.data());
              dispatch(setSingleItem(doc.data()));
          } else {
              // doc.data() will be undefined in this case
              console.log("No such document!");
          }
      }).catch( error => {
          console.log("Error getting document:", error);
          dispatch(fetchItemsFailed());
      });
  }
};