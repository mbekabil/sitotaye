import React, {Component} from 'react';
import {connect} from 'react-redux';

import Spinner from '../../../components/UI/Spinner/Spinner';
import withErrorHandler from '../../../hoc/withErrorHandler/withErrorHandler';
import * as actions from '../../../store/actions/index';
import axios from '../../../axios-orders';
import classes from './../Gifts.css';


class SingleGift extends Component {

    componentDidMount () {
        console.log(this.props.match.params.id);
        this.props.onSetSingleItem(this.props.match.params.id);
    }

    render () {
        let returnHtml = <Spinner/>;
        const placeholder = require('../../../assets/images/placeholder.png');
       // const thumbnail = require('../../assets/images/'+ props.ctg + '/'+ props.spec + '/' + props.thumbnail);

       if(this.props.item) {
            returnHtml = (
                <div className={classes.Gifts}>
                    <img src={placeholder} />
                    <span className={classes.Name}> {this.props.name} ({this.props.price}€)</span>
                    <div className={classes.Desc}> {this.props.description}</div>
                </div>
            );
        }
        return (
            <div className={classes.Gifts}>
                {returnHtml}
            </div>
        )
    }
}
const mapStateToPros = state => {
    return {
        item: state.gifts.singleItem,
        error: state.gifts.error,
        isAuthenticated: state.auth.token !== null
    }
};

const mapDispatchToProps = dispatch => {
    return {
        onSetSingleItem: (id) => dispatch(actions.setSingleGift(id)),
        onSetAuthRedirectPath: (path) => dispatch(actions.setAuthRedirectPath(path))
    }
};

export default connect(mapStateToPros, mapDispatchToProps)(withErrorHandler(SingleGift, axios));