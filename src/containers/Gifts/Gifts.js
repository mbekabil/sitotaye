import React, {Component} from 'react';
import {connect} from 'react-redux';

import Aux from '../../hoc/Aux';
import Modal from '../../components/UI/Modal/Modal';
import Spinner from '../../components/UI/Spinner/Spinner';
import withErrorHandler from '../../hoc/withErrorHandler/withErrorHandler';
import * as actions from '../../store/actions/index';
import axios from '../../axios-orders';
import Gift from '../../components/Gift/Gift';
import classes from './Gifts.css';


class Gifts extends Component {

    componentDidMount () {
        this.props.onInitItems();
    }

    render () {
        let returnHtml = <Spinner/>;
        if(this.props.items) {
            console.log(this.props.items)
            returnHtml = this.props.items.map( gift => (
                <Gift
                    key={gift.id}
                    data={gift}
                    path={this.props.match.path}/>
                ));
        }
        return (
            <div className={classes.Gifts}>
                {returnHtml}
            </div>
        )
    }
}
const mapStateToPros = state => {
    return {
        items: state.gifts.items,
        error: state.gifts.error,
        isAuthenticated: state.auth.token !== null
    }
};

const mapDispatchToProps = dispatch => {
    return {
        onInitItems: () => dispatch(actions.initGifts()),
        onSetAuthRedirectPath: (path) => dispatch(actions.setAuthRedirectPath(path))
    }
};

export default connect(mapStateToPros, mapDispatchToProps)(withErrorHandler(Gifts, axios));